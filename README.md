ovirtvmnicprops
===============

Returns Network Information from a running VM in oVirt/RHEV.
Pass valid JSON as stdin, or run with arguments specified below.

The original purpose of this is to pass as external data source with Terraform to correct shortcomings in oVirt's provider:
https://registry.terraform.io/providers/oVirt/ovirt

https://registry.terraform.io/providers/hashicorp/external

JSON usage:
```
cat file.json | ovirtvmnicprops
```
example file.json:
```
{
	"api": "https://ovirt.engine.url/ovirt-engine/api",
	"username": "user@domain",
	"password": "sup3rs3kr1t",
	"vm_id": "51e02a8e-2ced-47b8-86eb-8beb9358bce8",
	"nic_id": "3be69aef-8005-41f5-99f5-cec5b6ae65ea",
	"ipv4": true

}
```
Command line usage:
```
ovirtvmnicprops [-h] [--cert CERT] [--noverify] [--ipv4] [--ipv6] [--mac] [--json]
                       api username password vm_id nic_id

positional arguments:
  api          oVirt engine API url
  username     oVirt engine username
  password     oVirt engine password
  vm_id        ID of VM
  nic_id       ID of NIC

options:
  -h, --help   show this help message and exit
  --cert CERT  Path to TLS cert
  --noverify   Disable strict TLS check
  --ipv4       List returned IPv4 address(es)
  --ipv6       List returned IPv6 address(es)
  --mac        List returned MAC address(es)
  --json       Output as JSON

```