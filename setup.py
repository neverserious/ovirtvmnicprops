from setuptools import setup

setup(
	name='ovirtvmnicprops',
	version='0.0.1',
	description='Script to query oVirt API for VM NIC properties',
	url='https://gitlap.com/neverserio.us/ovirtvmnicprops',
	author='Robert Tongue',
	author_email='phunyguy@neverserio.us',
	install_requires=[
		'ipaddress',
		'macaddress',
		'requests',
		'argparse',
		'bs4'
	],
	scripts= [
		'bin/ovirtvmnicprops'
	],
)
